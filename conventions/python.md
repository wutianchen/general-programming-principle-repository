# Conventions

## pep8

* Use normal rules for colons, that is, no space before and one space after a colon: text: str.
* Use spaces around the = sign when combining an argument annotation with a default value: align: bool = True.
* Use spaces around the -> arrow: def headline(...) -> str.
* use 4 spaces for indentation instead of tab (Python 3 doesn't allow mixing tabs and spaces for indentation)


## single quotation vs double quotation

* Double quotes for text
* Single quotes for anything that behaves like an identifier
* Double quoted raw string literals for regexps
* Tripled double quotes for docstrings

[single quotation vs double quotation](https://www.geeksforgeeks.org/single-and-double-quotes-python/)

counter-example: 
* https://github.com/psf/requests


## Blank Lines

* top-level function and classes are separated by two blank lines. Method definitions inside classes should be separated by one blank line

## Imports

Order:

* Standard library imports
* Related third-party imports
* Local application/library specific imports




# Reference

* [PEP-8 Tutorial: Code Standards in Python](https://www.datacamp.com/community/tutorials/pep8-tutorial-python-code?utm_source=adwords_ppc&utm_campaignid=898687156&utm_adgroupid=48947256715&utm_device=c&utm_keyword=&utm_matchtype=b&utm_network=g&utm_adpostion=&utm_creative=229765585183&utm_targetid=aud-299261629574:dsa-429603003980&utm_loc_interest_ms=&utm_loc_physical_ms=9060719&gclid=CjwKCAjw7diEBhB-EiwAskVi126sDoXzUkTWXMnkcNzKLQiiLNIentU_4YtIQhE96v7lnmIsKktLNRoCs_kQAvD_BwE)