# High Performance Computing



## Julia

tbd






## Concurrent Computing


### Library

* [dask](https://dask.org/)
* [multiprocessing](https://docs.python.org/3/library/multiprocessing.html)
* [joblib](https://joblib.readthedocs.io/en/latest/parallel.html)
* [pandarallel](https://github.com/nalepae/pandarallel)
* [tqdm](https://github.com/tqdm/tqdm)
* [spark](https://spark.apache.org/)


### Reference

* [Using joblib to speed up your Python pipelines](https://towardsdatascience.com/using-joblib-to-speed-up-your-python-pipelines-dd97440c653d)


## Others Cython, Numba, PyPy

tbd


## TODO

* [Efficient Pandas: Using Chunksize for Large Data Sets](https://medium.com/towards-artificial-intelligence/efficient-pandas-using-chunksize-for-large-data-sets-c66bf3037f93)
* [Large, persistent DataFrame in pandas](https://stackoverflow.com/questions/11622652/large-persistent-dataframe-in-pandas/12193309#12193309)
* [Regex Performance in Python](https://towardsdatascience.com/regex-performance-in-python-873bd948a0ea)
* [Speed up millions of regex replacements in Python 3](https://stackoverflow.com/questions/42742810/speed-up-millions-of-regex-replacements-in-python-3/42747503#42747503)
* [Reading large CSV files using Pandas](https://medium.com/@lsriniv/reading-large-csv-files-using-pandas-7659baed6c27)
* [Processing large files using python](https://www.blopig.com/blog/2016/08/processing-large-files-using-python/)
* [CPU over-subscription by joblib.Parallel due to BLAS](https://martin-becker.net/blog/cpu-over-subscription-by-joblib-parallel-due-to-blas/)
* [Easy distributed training with Joblib and dask](https://tomaugspurger.github.io/distributed-joblib.html)
* [Make your Pandas apply functions faster using Parallel Processing](https://towardsdatascience.com/make-your-own-super-pandas-using-multiproc-1c04f41944a1)
* [How to Reduce the Size of a Pandas Dataframe in Python](https://towardsdatascience.com/how-to-reduce-the-size-of-a-pandas-dataframe-in-python-7ed6e4269f88)
* [Speed in Python](https://www.practicaldatascience.org/html/performance_understanding.html)
* [Solving Performance Issues](https://www.practicaldatascience.org/html/performance_solutions.html)
