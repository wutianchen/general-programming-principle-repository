# General Programming Principle Repository

* [Why Isn't Functional Programming the Norm? – Richard Feldman](https://www.youtube.com/watch?v=QyJZzq0v7Z4)


## Best Practice



* [EAFP](https://docs.python.org/3/glossary.html#term-eafp) vs [LBYL](https://docs.python.org/3/glossary.html#term-lbyl)

Easier to ask for forgiveness than permission. This common Python coding style assumes the existence of valid keys or attributes and catches exceptions if the assumption proves false. This clean and fast style is characterized by the presence of many try and except statements. The technique contrasts with the LBYL style common to many other languages such as C.

The Python community uses an EAFP (easier to ask for forgiveness than permission) coding style. This coding style assumes that needed variables, files, etc. exist. Any problems are caught as exceptions. This results in a generally clean and concise style containing a lot of try and except statements.

[generalization of exceptions](https://en.wikipedia.org/wiki/Exception_handling#Condition_systems)


#### pylint (W0102)

https://docs.quantifiedcode.com/python-anti-patterns/correctness/mutable_default_value_as_argument.html


## Books and Blogs

* [Python Website](https://www.python.org/)
* [Practical Data Science](https://www.practicaldatascience.org/html/index.html)
* [Real Python](https://realpython.com/instance-class-and-static-methods-demystified/)
* [Inside Python Virutal Machine](https://leanpub.com/insidethepythonvirtualmachine/read#leanpub-auto-introduction)
* [Python Shorts](https://towardsdatascience.com/tagged/python-shorts)
* [The Hitchhiker’s Guide to Python!](https://docs.python-guide.org/)
* [The Little Book of Python Anti-Patterns](https://docs.quantifiedcode.com/python-anti-patterns/index.html)
* [Awesome Python](https://github.com/vinta/awesome-python)
* [PEP](https://www.python.org/dev/peps/)


## Toolkit

* [jinjasql](https://pypi.org/project/jinjasql/)
* [Missingno: visualize missing value and messy data](https://medium.com/@shivama205/dependency-injection-python-cb2b5f336dce)
* [mypy: optional static typing for python](http://mypy-lang.org/)
* [dask](https://dask.org/)
* [Paramiko](http://docs.paramiko.org/en/stable/#)
* [requests](https://requests.readthedocs.io/en/master/)
* [datetime , pytz]
* [itables: interactive pandas dataframe](https://pypi.org/project/itables/)




* tool converting curl into python: https://curl.trillworks.com/
* requests vs urllib3: https://dev.to/zenulabidin/python-http-at-lightspeed-part-2-urllib3-and-requests-476
* Gnocchi – Metric as a Service: https://gnocchi.xyz/


## Language Features

* [f-string](https://realpython.com/python-f-strings/#arbitrary-expressions)

