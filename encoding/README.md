# Encoding

* [chardet](https://chardet.readthedocs.io/en/latest/usage.html)


sometimes one file can contain multiple encoding (https://stackoverflow.com/questions/9885071/file-using-multiple-encoding)

how to detect encoding of file automatically

```python
import chardet
chardet.detect(rawdata) {'encoding': 'EUC-JP', 'confidence': 0.99}
```