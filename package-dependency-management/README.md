# Package, Dependency Management

![History of Package and Dependency Management](../images/packaging.PNG)


| Tool | Description|
|------|------------|
| anaconda | https://www.anaconda.com/ |
| virtualenv | https://pypi.org/project/virtualenv/ |
| python poetry | https://python-poetry.org/docs/ |
| pip | https://pip.pypa.io/en/stable/reference/pip_install/ |
| pipenv | https://pipenv.pypa.io/en/latest/ |

# Virtual Environment

#### venv / pyvent

* same python version
* different package dependencies

#### pyenv

* multiple versions of python

#### pyenv-virutalenv

* virtual environment for python2

#### conda

alternative to pyenv


```bash
# install virtual venv
pip install --user virtualenv

# create a virutal environment
# python3
# cd into your project directory root
# "env" is the virtual environment name here
python3 -m venv env 

# activate virtual environment
source env/bin/activate
```

# Reference

* [Python Packaging Authority](https://www.pypa.io/en/latest/)
* [Poetry: "dependency management and packaging made easy"](https://www.youtube.com/watch?v=QX_Nhu1zhlg)
* ["Publishing well-formed Python packages" - Julin S (PyConline AU 2020)](https://www.youtube.com/watch?v=_b8D4v7YIME)
* [Publishing (Perfect) Python Packages on PyPi](https://www.youtube.com/watch?v=GIF3LaRqgXo)
* [Python Tutorial: Pipenv - Easily Manage Packages and Virtual Environments](https://www.youtube.com/watch?v=zDYL22QNiWk)
* [How to manage multiple Python versions and virtual environments](https://www.freecodecamp.org/news/manage-multiple-python-versions-and-virtual-environments-venv-pyenv-pyvenv-a29fb00c296f/#:~:text=project%20specific%20decision.-,pyenv%2Dvirtualenv,for%20all%20versions%20of%20Python)
* [Managing Multiple Python Versions With pyenv](https://realpython.com/intro-to-pyenv/)