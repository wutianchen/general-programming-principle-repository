# Tpying



mypy: https://mypy.readthedocs.io/en/stable/cheat_sheet_py3.html
Typeshed: Typeshed is a Github repository that contains type hints for the Python standard library, as well as many third-party packages. Typeshed comes included with Mypy so if you are using a package that already has type hints defined in Typeshed, the type checking will just work (https://github.com/python/typeshed)
pyre: https://pyre-check.org/
pytype: https://github.com/google/pytype


## Ways to introduce tying into Python code

> Use annotations if you can, use type comments if you must. Stub files will work in any version of Python, at the expense of having to maintain a second set of files. In general, you only want to use stub files if you can’t change the original source code


* type annotation
* type comment (python2 legacy)
* stub files (adding types to third party library)



## Articles

* [Python Type Checking (Guide)](https://realpython.com/python-type-checking/)
* [Python's Mypy--Advanced Usage](https://www.linuxjournal.com/content/pythons-mypy-advanced-usage)