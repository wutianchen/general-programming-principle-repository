# Functional Programming

> The essence of functional programming is to chunk a big problem problem into easily solvable small pieces. From engineering perspective, it is building some tools to solve one aspect of the problem and compose them later on


* immutability
* function as first order object
* closure

pyspark is mostly functional programming

# Reference


* Book: real world Haskell
* [Calen Pennington Immutable Programming Writing Functional Python PyCon 2017](https://www.youtube.com/watch?v=_OLEVvjrIj8)
* [Daniel Kirsch - Functional Programming in Python](https://www.youtube.com/watch?v=r2eZ7lhqzNE)
* [Stupid Itertools Tricks For Data Science](https://github.com/joelgrus/stupid-itertools-tricks-pydata)