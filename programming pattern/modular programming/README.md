# Modular Programming

The important of namespace (encapsulation), messaging

* Object Oriented Programming


# Class

## class methods

| method type     | Modify object instance state   | Modify class state  |
| ------------- |:-------------:| -----:|
| instance method      | + | + |
| class method      | -  |   + |
| static method | -  |  - |

* [Python's Instance, Class, and Static Methods Demystified](https://realpython.com/instance-class-and-static-methods-demystified/)
* [The definitive guide on how to use static, class or abstract methods in Python](https://julien.danjou.info/guide-python-static-class-abstract-methods/)
* static methods are used to do some utility task
* class methods are used for factory methods

```python
class MyClass:
    ''' 
    Instance method has a mandatory first attribute self which represent the instance itself. 
    Instance method must be called by a instantiated instance.
    '''
    def method(self):
        return 'instance method called', self
    
    '''
    Class method has a mandatory first attribute cls which represent the class itself. 
    Class method can be called by an instance or by the class directly. 
    Its most common using scenario is to define a factory method.
    '''
    @classmethod
    def class_method(cls):
        return 'class method called', cls
    
    '''
    Static method doesn’t have any attributes of instances or the class. 
    It also can be called by an instance or by the class directly. 
    Its most common using scenario is to define some helper or utility functions which are closely relative to the class.
    '''
    @staticmethod
    def static_method():
        return 'static method called'


obj = MyClass()
print(obj.method())
print(obj.class_method()) # MyClass.class_method()
print(obj.static_method()) # MyClass.static_method()
```

## Mixins

* https://www.thedigitalcatonline.com/blog/2020/03/27/mixin-classes-in-python/
* https://www.thedigitalcatonline.com/blog/2013/10/28/digging-up-django-class-based-views-1/
* https://www.thedigitalcatonline.com/blog/2013/12/11/digging-up-django-class-based-views-2/
* https://www.thedigitalcatonline.com/blog/2014/02/14/digging-up-django-class-based-views-3/




# Design Pattern

there are two types of inheritance: interface inheritance and implementation inheritance


## Composition over Inheritance

* [Ariel Ortiz - The Perils of Inheritance: Why We Should Prefer Composition - PyCon 2019](https://www.youtube.com/watch?v=YXiaWtc0cgE)


## Dependency Injection

* [Dependency Injection: Python](https://medium.com/@shivama205/dependency-injection-python-cb2b5f336dce)


## Solid

https://en.wikipedia.org/wiki/SOLID

* [Single-responsibility principle](https://en.wikipedia.org/wiki/Single-responsibility_principle)
* [Open–closed principle](https://en.wikipedia.org/wiki/Open%E2%80%93closed_principle)
* [Liskov substitution principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle)
* [Interface segregation principle](https://en.wikipedia.org/wiki/Interface_segregation_principle)
* [Dependency inversion principle](https://en.wikipedia.org/wiki/Dependency_inversion_principle)

## KISS

tbd


## DRY

tbd